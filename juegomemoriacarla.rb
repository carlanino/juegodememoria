arreglo = [] #para rubi las matrices son arreglos asi que siguiendo el ejemplo de clase creo un arreglo vacio 
while(arreglo.length < 20)
  repetido = false      #indicador asumiento que no esta repetido         
  aleatorio = rand(10)                           # se crea asi xq tienen que estar repetidos del 1 al 10 repetidos 
  for i in (0..arreglo.length - 1)  # este for es para que se repita solo dos veces el aleatorio 
    if(arreglo.count(aleatorio) >= 2)  # condicional dice cuantas veces existe el mismo numero en el arreglo 
      repetido = true
    end
  end
  if(repetido == false)
    arreglo[arreglo.length] = aleatorio
  end
end
matriz = Array.new(4) { Array.new(5, 0) }    #matriz de filas y columnas pedidas 
=begin
matriz = [] # otra forma de crear la matriz
for i in 0..3 do
   matriz[i] = []
      for j in 0..4 do                                     #
         matriz[i][j] = rand(10)
      end
end
=end
contador = 0                             #llena la matriz con los elementos del arreglo de arriba, se pone para determinar los espacios del arreglo, llena la matriz con los arreglos q tenemos 
for i in 0..matriz.length - 1 do #recorre fila 4 veces 
  for j in 0..matriz[i].length - 1 do #recorre columna 5 veces 
    matriz[i][j] = arreglo[contador]  #insercion filas y columnas, insertar valor de la posicion del arreglo en la posicion del indice de la matriz 
    contador = contador + 1  #este ciclo se va a recorrer en total 20 
  end
end
#print "#{matriz}\n\n" esto es para imprimer la matriz real 
turno = 0
puntos = 0
valor_carta_uno = []
valor_carta_dos = []
numeros_permitidos_columnas = %W[1 2 3 4 5]
numeros_permitidos_filas = %W[1 2 3 4]

puts "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°"
puts "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°"
puts "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°° BIENVENIDO AL JUEGO DE MEMORIA DE CARLA °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°"                       #inicia la diversion
puts "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°"
puts "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°\n"

puts "Las reglas del juego son simples, el jugador tiene un turno para levantar 2 cartas si estas coniciden"
puts "quedan descubiertas, en caso contrario quedaran ocultas nuevamente"
puts "para descubrir las cartas deberas indicarnos 4 elementos, la fila y la comuna de la primera carta y la fila y la columna de la segunda carta"
puts "por ejemplo quieres levantar la carta que esta en la 3 columna y fila 5, pues nos indicas en la columna el numero 3 y para la fila el numero 5"
puts "ten en cuenta que el juego solo acepta opciones de numero del 1 al 5, un numero o letra distinto de este rango no esta permitido, por ejemplo la posicion columna 5 fila 6 no existe"
puts "¡Que te diviertas!"
puts "PUNTUACION: #{puntos}"
puts "TURNO: #{turno}\n"

fila_uno = %w[* * * * *]
fila_dos = %w[* * * * *]
fila_tres = %w[* * * * *]
fila_cuatro = %w[* * * * *]                                                          #es una especie de trama inteligente, crear arreglos vacios que funcionan como mascara 
puts fila_uno * ' '
puts fila_dos * ' '
puts fila_tres * ' '
puts fila_cuatro * ' '



while fila_uno.include?('*') == true || fila_dos.include?('*') == true || fila_tres.include?('*') == true || fila_cuatro.include?('*') == true    # sintaxis, verifica si en el arreglo de fila  existe caracter *
    turno = turno + 1

    # recibiendo la primera carta
    print "Dame un valor de columna para la carta 1: "
    columna_carta_1 = gets.chomp
    while numeros_permitidos_columnas.include?(columna_carta_1) == false    # la condicion aca es q es falso, mientras el usuario ponga un valor no permitido esto se repetira hasta q ponga un valor permitido 
        print "No es una posicion o número válido de columna para carta 1, inetenta de nuevo: "
        columna_carta_1 = gets.chomp
    end
    columna_carta_1 = columna_carta_1.to_i - 1

    print "Dame un valor de fila para la carta 1: "
    fila_carta_1 = gets.chomp
    while numeros_permitidos_filas.include?(fila_carta_1) == false
        print "No es una posicion o número válido de fila para carta 2, inetenta de nuevo:  "
        fila_carta_1 = gets.chomp
    end
    fila_carta_1 = fila_carta_1.to_i - 1

    valor_carta_uno[turno] = matriz[fila_carta_1][columna_carta_1]
    case fila_carta_1
    when 0
        fila_uno[columna_carta_1] = valor_carta_uno[turno]
    when 1
        fila_dos[columna_carta_1] = valor_carta_uno[turno]
    when 2
        fila_tres[columna_carta_1] = valor_carta_uno[turno]
    when 3
        fila_cuatro[columna_carta_1] = valor_carta_uno[turno]
    end

    puts fila_uno * ' '
    puts fila_dos * ' '
    puts fila_tres * ' '
    puts fila_cuatro * ' '


    # recibiendo la segunda carta
    print "Dame un valor de columna para la carta 2: "
    columna_carta_2 = gets.chomp
    while numeros_permitidos_columnas.include?(columna_carta_2) == false
        print "No es una posicion o número válido de columna para carta 2, inetenta de nuevo: "
        columna_carta_2 = gets.chomp
    end
    columna_carta_2 = columna_carta_2.to_i - 1

    print "Dame un valor de fila para la carta 2: "
    fila_carta_2 = gets.chomp
    while numeros_permitidos_filas.include?(fila_carta_2) == false
        print "No es una posicion o número válido de fila para carta 2, inetenta de nuevo: "
        fila_carta_2 = gets.chomp
    end
    fila_carta_2 = fila_carta_2.to_i - 1

    valor_carta_dos[turno] = matriz[fila_carta_2][columna_carta_2]

    case fila_carta_2
    when 0
        fila_uno[columna_carta_2] = valor_carta_dos[turno]
    when 1
        fila_dos[columna_carta_2] = valor_carta_dos[turno]
    when 2
        fila_tres[columna_carta_2] = valor_carta_dos[turno]
    when 3
        fila_cuatro[columna_carta_2] = valor_carta_dos[turno]
    end

    puts fila_uno * ' '
    puts fila_dos * ' '
    puts fila_tres * ' '
    puts fila_cuatro * ' '

    if valor_carta_uno[turno] == valor_carta_dos[turno]

        puts "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°"
        puts "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°"
        puts "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°° BIENVENIDO AL JUEGO DE MEMORIA DE CARLA °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°"
        puts "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°"
        puts "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°\n"
        puts "Felicidades!, Haz descubierto el par, continua hasta que las consigas todas"
        puts "PUNTUACION: #{puntos}"
        puts "TURNO: #{turno}\n"

        puts fila_uno * ' '
        puts fila_dos * ' '
        puts fila_tres * ' '
        puts fila_cuatro * ' '


        puntos = puntos + 5                   
        
    else 

        valor_carta_uno[turno] = '*'
        valor_carta_dos[turno] = '*'
        case fila_carta_1
        when 0
            fila_uno[columna_carta_1] = valor_carta_uno[turno]
        when 1
            fila_dos[columna_carta_1] = valor_carta_uno[turno]
        when 2
            fila_tres[columna_carta_1] = valor_carta_uno[turno]
        when 3
            fila_cuatro[columna_carta_1] = valor_carta_uno[turno]
        end

        case fila_carta_2
        when 0
            fila_uno[columna_carta_2] = valor_carta_dos[turno]
        when 1
            fila_dos[columna_carta_2] = valor_carta_dos[turno]
        when 2
            fila_tres[columna_carta_2] = valor_carta_dos[turno]
        when 3
            fila_cuatro[columna_carta_2] = valor_carta_dos[turno]
        end

        def clear_screen  #si lo pongo sin funcion se va a borrar la pantalla sin mostrar los elementos que imprimio, empatia con el usuario
          
              system('clear')

        end

        puts "No son iguales, presiona enter para continuar jugando y voltear las cartas"
        $stdin.gets
        clear_screen

        puts "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°"
        puts "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°"
        puts "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°° BIENVENIDO AL JUEGO DE MEMORIA DE CARLA °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°"
        puts "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°"
        puts "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°\n"
        puts "No son iguales, continuar jugando"
        puts "PUNTUACION: #{puntos}"
        puts "TURNO: #{turno}\n"

        puts fila_uno * ' '
        puts fila_dos * ' '
        puts fila_tres * ' '
        puts fila_cuatro * ' '

        

    end
    
    
end

puts "El juago a finalizado"
puts "Obtuviste un total de puntos igual a #{puntos}"
puts "Con un total de #{turno} turnos"
